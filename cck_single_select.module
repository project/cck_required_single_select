<?php

/**
 * @file
 * Main file of the "CCK Single Select" module.
 */

/**
 * Defining global constants
 */
//define('CCK_SINGLE_SELECT_DEBUG', TRUE);
  define('CCK_SINGLE_SELECT_DEBUG', FALSE);
  define('CCK_SINGLE_SELECT_MODULE_FULLNAME', 'CCK Single Select');
  define('CCK_SINGLE_SELECT_MODULE_HOMEPAGE', 'https://drupal.org/project/cck_single_select');
  define('CCK_SINGLE_SELECT_AUTHOR_NAME', 'Robert Allerstorfer');
  define('CCK_SINGLE_SELECT_AUTHOR_HOMEPAGE', 'https://drupal.org/user/405360');
  define('CCK_SINGLE_SELECT_MY_PATH', dirname(__FILE__) . '/');
  define('CCK_SINGLE_SELECT_MY_URL', drupal_get_path('module', 'cck_single_select') . '/');

module_load_include('inc', 'cck_single_select', 'includes/cck_single_select.help');
module_load_include('inc', 'cck_single_select', 'includes/cck_single_select.menu');

/**
 * Implementation of hook_theme().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_theme/6
 *
 * Registers the theme hook "cck_single_select"
 * to provide customization to the non-value first option.
 */
function cck_single_select_theme($existing, $type, $theme, $path) {
  module_load_include('inc', 'cck_single_select', 'includes/cck_single_select.theme');
  return _cck_single_select_theme_array($existing, $type, $theme, $path);
}

/**
 * Implementation of hook_form_alter().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_form_alter/6
 *
 * Validates the appropriate elements.
 */
function cck_single_select_form_alter(&$form, &$form_state, $form_id) {
  // Add the non-submittable option to the appropriate fields:
    if (!empty($form['#field_info']) && is_array($form['#field_info'])) {
      // Cycle through CCK fields:
      foreach ($form['#field_info'] as $key => $value) {
        if (!$value['multiple']) {
          // The field is single-value.
          if ($value['widget']['type'] == 'optionwidgets_select'
            || preg_match('/^(node|user)reference_select$/', $value['widget']['type'])
          ) {
            // Select list of one the following CCK types:
            // "Text" (optionwidgets_select),
            // "Node reference" (nodereference_select), or
            // "User reference" (userreference_select).
            if ($value['required']) {
              // The field is required.
              // Temporarily unset 'required' during build phase:
              // => Adds first option '- None -'
              $form[$key]['#was_required'] = $value['required'];
              $form[$key]['#required'] = 0;
              $form['#field_info'][$key]['required'] = 0;
            }
            $form[$key]['#after_build'] = array(_cck_single_select_after_build);
          }
        }
      }
    }
}

/**
 * Helper function to reset the element to be required.
 * https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/6#after_build
 *
 * Defined in the field's #after_build attribute.
 */
function _cck_single_select_after_build($element, &$form_state) {
  $required = FALSE;
  if (isset($element['#was_required']) && $element['#was_required']) {
    $required = TRUE;
    $element['#required'] = $element['#was_required'];
  }
  $first_option = preg_replace("/\n/", '', check_plain(theme('cck_single_select', array('required' => $required))));
  if ($element['#type'] == 'optionwidgets_select') {
    $required && $element['value']['#required'] = $element['#was_required'];
    $first_option_old = reset($element['value']['#options']);
    $element['value']['#options'][''] = $first_option;
  }
  elseif (preg_match('/^(node|user)reference_select$/', $element['#type'])) {
    foreach (element_children($element) as $key1) {
      // https://api.drupal.org/api/drupal/includes!common.inc/function/element_children/6
      if ($element[$key1]['#type'] == 'optionwidgets_select') {
        // $element['nid']
        $required && $element[$key1]['#required'] = $element['#was_required'];
        foreach (element_children($element[$key1]) as $key2) {
          if ($element[$key1][$key2]['#type'] == 'select') {
            // $element['nid']['nid']
            $required && $element[$key1][$key2]['#required'] = $element['#was_required'];
            $first_option_old = reset($element[$key1][$key2]['#options']);
            $element[$key1][$key2]['#options'][''] = $first_option;
          }
        }
      }
    }
  }
  if (CCK_SINGLE_SELECT_DEBUG === TRUE) {
    print '<pre style="display: none">';
    print "\n---------- " . $element['#name'] . " ----------\n";
    print_r($element);
    print '</pre>';
  }
  return $element;
}
