<?php

/**
 * @file
 * Part of the "CCK Single Select" module that will be included for displaying help.
 */

/**
 * Implementation of hook_help().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_help/6
 */
function cck_single_select_help($path, $arg) {
  switch ($path) {

    case 'admin/help#cck_single_select':
      $link_module = l(
        CCK_SINGLE_SELECT_MODULE_FULLNAME,
        CCK_SINGLE_SELECT_MODULE_HOMEPAGE,
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Homepage of') . ' ' . CCK_SINGLE_SELECT_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );
      $link_author = l(
        CCK_SINGLE_SELECT_AUTHOR_NAME,
        CCK_SINGLE_SELECT_AUTHOR_HOMEPAGE,
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Author of') . ' ' . CCK_SINGLE_SELECT_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );

      $link_readme = l(
        'README.txt',
        'admin/cck_single_select/readme',
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => 'README.txt ' . t('of') . ' ' . CCK_SINGLE_SELECT_MODULE_FULLNAME . ' ' . t('module'),
          ),
        )
      );

      $placeholders = array(
        '!link_module' => $link_module,
        '!link_author' => $link_author,
        '!link_readme' => $link_readme,
      );

      return "\n" . t(
        '<p>Welcome to the Help page of Drupal\'s !link_module module.<br />'
          . 'Developer of this branch: !link_author.</p>'
          . '<p>Complete documentation is available in the module\'s !link_readme file.</p>',
        $placeholders
      );
      break;

    case 'admin/cck_single_select/readme':
      $path_readme = CCK_SINGLE_SELECT_MY_PATH . 'README.txt';
      if (is_readable($path_readme)) {
        $contents_readme = file_get_contents($path_readme);
        if (!drupal_strlen($contents_readme)) {
          $contents_readme = t('Error: File could not be read!');
        }
      }
      else {
        $contents_readme = t('Error: File is not readable!');
      }
      return "\n" . '<p><pre style="padding: 10px; border: 1px solid;">' . check_plain($contents_readme) . '</pre></p>';
      break;
  }
}
