<?php

/**
 * @file
 * Part of the "CCK Single Select" module that will be included for registering pathes.
 */

/**
 * Implementation of hook_menu().
 * https://api.drupal.org/api/drupal/developer!hooks!core.php/function/hook_menu/6
 */
function cck_single_select_menu() {
  $items = array();
  // Item 1:
  $items['admin/cck_single_select/readme'] = array(
    'file' => 'includes/cck_single_select.menu.inc',
    'page callback' => 'cck_single_select_readme',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
    'title' => CCK_SINGLE_SELECT_MODULE_FULLNAME . ': \'README.txt\'',
    'title callback' => FALSE,
  );
  return $items;
}

/**
 * Page callback function for Item 1 of function above.
 */
function cck_single_select_readme() {
  $link_help = l(
    CCK_SINGLE_SELECT_MODULE_FULLNAME,
    'admin/help/cck_single_select',
    array(
      'attributes' => array(
        'title' => t('Help for') . ' ' . CCK_SINGLE_SELECT_MODULE_FULLNAME . ' ' . t('module'),
      ),
    )
  );

  $placeholders = array(
    '!link_help' => $link_help,
  );

  return "\n" . '<p><ul><li>' . t('Help for the !link_help module', $placeholders) . '</li></ul></p>';
}
