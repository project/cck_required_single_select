################################################################################
#                                                                              #
#          CCK Single Select                                                   #
#          Version 6.x-2.0-dev (2013-0x-xx)                                    #
#                                                                              #
#          A module for Drupal 6.x                                             #
#                                                                              #
################################################################################

Web page
========

The project homepage is
  https://drupal.org/project/cck_required_single_select

Authors
=======

Maintainer (since 2012-02-24):

- Robert Allerstorfer (roball)
    https://drupal.org/user/405360

Original author (discontinued maintainership):

- Chris Webster (cwebster)
    https://drupal.org/user/374820

Overview
========

This module extends the functionality provided by four modules coming with
"Content Construction Kit (CCK)" (https://drupal.org/project/cck):

- "Option Widgets"
- "Text"
- "Node Reference"
- "User Reference"

It acts when a CCK field

- of type "Text", "Node reference" or "User reference"
- with the widget "Select list"

is added to a content type,

- and the Number of values is kept to 1(one).

Such a field is a single-value select list. CCK would natively display such a
single-value select list as follows:

- If the field is required, the first selectable option is pre-selected. This
  would allow the end-user to successfully submit a form without actually
  selecting anything from a required select list, effectively making the
  "required" property obsolete.
- If the field is not required, a non-value option "- None -" will be added on
  top.

"CCK Single Select" (this module) changes the first (default) option of each
single-value select list as follows:

- If the field is required, a non-submittable customizable option will be added
  on top (defaults to "- Please select an option -"). This forces the end-user
  to actively select one of the provided options in order to successfully
  submit the form.
- If the field is not required, the non-value option on top will be made
  customizable (defaults to "- No selection -").

Details
=======

CCK natively only adds a non-value option (per default "- None -") on top of a
single-value select list, if the field is *not* required (optional). "CCK
Single Select" expands that functionality to *required* single-value select
lists.

The texts of the non-value first option for both required and optional fields
are customizable and translatable via an overridable PHP template file.

Requirements
============

- The following contributed Drupal 6.x module:

    * Content Construction Kit (CCK)
        https://drupal.org/project/cck
        6.x-3.x (6.x-2.x may also work, but has not been tested)
        [+] Option Widgets

        "CCK Single Select" only makes sense when one ore more of the
        following CCK modules are also enabled:
        [+] Text
        [+] Node Reference
        [+] User Reference

Installation
============

This module follows Drupal's standard module installation procedure.

If Drupal is running on a Unix server, the most convenient way to install the
module is doing it directly on the sh shell (via SSH). The commands to enter as
the root user may be something like:

[root@server ~]# cd /etc/drupal6/all/modules
[root@server modules]# rm -rf cck_single_select cck_single_select-*
[root@server modules]# wget \
> http://ftp.drupal.org/files/projects/cck_single_select-6.x-2.0.tar.gz
[root@server modules]# chmod 600 cck_single_select-6.x-2.0.tar.gz
[root@server modules]# tar -zxf cck_single_select-6.x-2.0.tar.gz
[root@server modules]# chown -R apache:apache cck_single_select

After installation, load the "Administer » Site building » Modules"
(admin/build/modules) page, tick the "CCK Single Select" checkbox within the
"CCK" group and press the [ Save configuration ] button. You should see a
message that the "CCK Single Select" module has been enabled.

Upgrade from version 6.x-1.x
============================

  1. Make sure both the "CCK Required Single Select" and "CCK Required Reference
     Select" modules are disabled.

  2. Remove the old module directory "cck_required_single_select":

[root@server modules]# rm -rf cck_required_single_select

  3. Follow the Installation instructions above.

  4. Run Drupal's database update utility ("update.php").

Configuration
=============

There is no configuration required. However, if you want to change the texts
of the non-value first option from the default (translatable) string
"- Please select an option -" for required or "- No selection -" for optional
fields, follow these steps:

  1. Copy the PHP template file "cck-single-select.tpl.php" from this module's
     "templates" sub directory into the directory of your active (default)
     theme.

  2. Open the copied file with a text editor. You will see how you can change
     the file in order to change the string of the variable $option. You can
     also set its value conditionally based on different sites and pathes.

  3. Once you have made changes to the template file, flush all caches on your
     Drupal site(s) using the theme the template has been provided to.

Uninstallation
==============

1. Go to "Administer » Site building » Modules" (admin/build/modules), untick
   the "CCK Single Select" checkbox within the "CCK" group and press the
   [ Save configuration ] button. You should see a message that the "CCK Single
   Select" module has been disabled.

2. Click the [ Uninstall ] tab (admin/build/modules/uninstall), tick the "CCK
   Single Select" checkbox and press the [ Uninstall ] button. On the next
   screen you must confirm uninstall by again pressing an [ Uninstall ] button.

3. Entirely remove the module's directory from the modules directory:

[root@server modules]# rm -rf cck_single_select

Contributors
============

Support for the CCK types "Node reference" and "User reference" originally
contributed by:

- Konrad Szymczak (kndr)
    https://drupal.org/user/132465
